﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TempleFinal.Models;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace TempleFinal.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult RegisterDetails()
        {
            //tblCustomerDetail oCust = new tblCustomerDetail();
            //DataSet ds = new DataSet();
            //string constr = ConfigurationManager.ConnectionStrings["TempleDbContext"].ConnectionString;
            //using (SqlConnection con = new SqlConnection(constr))
            //{
            //    using (SqlCommand cmd = new SqlCommand("Select * from tblCustmerDetails", con))
            //    {
            //        con.Open();
            //        SqlDataAdapter da = new SqlDataAdapter(cmd);
            //        da.Fill(ds);
            //        List<tblCustomerDetail> userlist = new List<tblCustomerDetail>();
            //        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            //        {
            //            tblCustomerDetail oCustomer = new tblCustomerDetail();
            //            oCustomer.first_name = Convert.ToString(ds.Tables[0].Rows[i]["firs_name"].ToString());
            //            oCustomer.middle_name = ds.Tables[0].Rows[i]["username"].ToString();
            //            oCustomer.last_name = ds.Tables[0].Rows[i]["education"].ToString();
            //            userlist.Add(oCustomer);
            //        }
            //        oCust.usersinfo = userlist;
            //    }
            //    con.Close();
            //}
            //return View(objuser);
            return View();
        }

        [HttpPost]
        public ActionResult RegisterDetails(tblCustomerDetail customer)
        {
            if (ModelState.IsValid)
            {
                using (TempleDbContext db = new TempleDbContext())
                {
                    db.dCustomerDetail.Add(customer);
                    db.SaveChanges();
                }
                ModelState.Clear();
                ViewBag.Message = customer.first_name + " " + customer.last_name;
            }
            return View();
        }


        public ActionResult Login()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Login(UserDetail user)
        {
            using (TempleDbContext db = new TempleDbContext())
            {
                var usr = db.dUserDetail.Single(u => u.user_name == user.user_name && u.password == user.password);
                if (usr != null)
                {
                    Session["UserId"] = usr.user_id.ToString();
                    Session["UserName"] = usr.user_name.ToString();
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "Username or Passowrd is wrong.");
                }

            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}