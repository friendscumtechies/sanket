﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using TempleFinal.Models;

namespace TempleFinal.Models
{
    public class TempleDbContext : DbContext
    {
        //public TempleDbContext() : base("UserDetails")  
        //{ }
        public DbSet<UserDetail> dUserDetail { get; set; }
        public DbSet<tblCustomerDetail> dCustomerDetail { get; set; }
    }
}